/*******************************************************************************
    This file is part of Tomat.

    Tomat is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tomat is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Tomat.  If not, see <http://www.gnu.org/licenses/>.

*******************************************************************************/

/* window.c */

#include "window.h"

WINDOW *create_newwin (int height, int width, int starty, int startx)
{
    WINDOW *local_win;
    
    local_win = newwin(height, width, starty, startx);
    box(local_win, 0, 0);	/* 0,0 gives default valuse for the  
				 * vertical and horizontal lines */
    wrefresh(local_win);	/* show that box */

    return local_win;
}

void destroy_win (WINDOW *local_win)
{
    /* box(local_win, ' ', ' '); : This won't procedure the desired
     * result of erasing the window. It will leave its four corners
     * and so an ugly remnant of window. */
    wborder(local_win, ' ', ' ',' ', ' ',' ', ' ',' ', ' ');
    /* the parameters taken are:
     * 1. win: the window on which to operate
     * 2. ls: character to be used for the left side of the window
     * 3. rs: character to be used for the right side of the window
     * 4. ts: character to be used for the top side of the window
     * 5. bs: character to be used for the bottom side of the window
     * 6. tl: character to be used for the top left corner of the window
     * 7. tr: character to be used for the top right corner of the window
     * 8. bl: character to be used for the bottom left corner of the window
     * 9. br: character to be used for the right corner of the window */
    wrefresh(local_win);
    delwin(local_win);
}
