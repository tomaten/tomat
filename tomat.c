/*******************************************************************************
    This file is part of Tomat.

    Tomat is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tomat is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Tomat.  If not, see <http://www.gnu.org/licenses/>.

*******************************************************************************/

#include <curses.h>
#include <string.h>

#include "window.h"

int main (void)
{
    WINDOW *status_win;
    int ch, row, col;
    const char msg[] = "Tomat - A Pomodoro App";

    initscr();
    start_color();
    /* init_pair(1, COLOR_CYAN, COLOR_BLACK); */
    raw();
    keypad(stdscr, TRUE);
    noecho();
    getmaxyx(stdscr, row, col);

    /* Create the title */
    mvprintw(1, (col-strlen(msg))/2, "%s", msg);
    refresh();

    /* Create a status window */
    status_win = create_newwin(STATUS_WIN_HEIGHT,col,row-STATUS_WIN_HEIGHT,0);
    ch = getchar();
    
    refresh();
    endwin();

    return 0;
}
