/*******************************************************************************
    This file is part of Tomat.

    Tomat is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tomat is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Tomat.  If not, see <http://www.gnu.org/licenses/>.

*******************************************************************************/

/* test_window.h */

#ifndef WINDOW_H
#define WINDOW_H

#include <curses.h>

#define STATUS_WIN_HEIGHT 3

/* Creates a new window */
WINDOW *create_newwin(
    int height, 
    int width, 
    int starty, 
    int startx
);

/* Destroys a window */
void destroy_win(
    WINDOW *local_win
);

#endif	/* WINDOW_H */
